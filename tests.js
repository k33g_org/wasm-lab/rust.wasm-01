
const wasm = require("./hello/pkg/hello")

console.log(
  wasm.handle(
    {
      text:"coucou",
      author:"sam"
    },
    {
      first: "Philippe",
      last: "Charriere"
    },
    ["one", "two", "three"]
  )
)

