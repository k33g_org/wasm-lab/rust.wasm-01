
use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct Question {
    pub text: String,
    pub author: String,
}

#[derive(Serialize, Deserialize)]
pub struct Answer {
    pub text: String,
    pub author: String,
    pub values: Vec<String>,
}

#[wasm_bindgen]
pub fn handle(value: JsValue, human: JsValue, some_values: JsValue) -> Result<JsValue, JsValue>  {

    let human_map: HashMap<String,String> = human.into_serde().unwrap();
    let some_data = match human_map.get("first") {
        Some(string) => format!("😁😡 first name: {}", string),
        _ => format!("no first name."),
    };

    let list_of_values: Vec<String> = some_values.into_serde().unwrap();
    

    let first_name: String = human_map.get("first").unwrap().to_string();
    let last_name: String = human_map.get("last").unwrap().to_string();


    // deserialize value (parameter) to question
    let question: Question = value.into_serde().unwrap();

    // serialize answer to JsValue
    let answer = Answer {
        text: String::from(format!("hello {} / {} {} | {}", question.text, first_name, last_name, some_data)),
        author: String::from("@k33g_org"),
        values: list_of_values,
    };

    //return JsValue::from_serde(&answer).unwrap()
    return Ok(JsValue::from_serde(&answer).unwrap())
}