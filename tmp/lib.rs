use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};

use std::collections::HashMap;
//use js_sys::JSON;

//use js_sys::{Map}; 
//use serde_wasm_bindgen::{Deserializer, Serializer, to_value, from_value};



#[derive(Serialize, Deserialize)]
pub struct Question {
    pub text: String,
    pub author: String,
}

#[derive(Serialize, Deserialize)]
pub struct Answer {
    pub text: String,
    pub author: String,
}


//https://www.fpcomplete.com/blog/serverless-rust-wasm-cloudflare/

/*
See https://dev.to/werner/practical-rust-web-development-front-end-538d

as param: 
body: Option<String>
let js_value = JsValue::from_str(&body_string);
*/

/*
use serde_json::json;
let restaurants = json!([
    {"name": "Le Ciel", "address": "17 rue Alexandre Dumas, 75011 Paris", "phone": "(33)664 441 416"},
    {"name": "A La Renaissance", "address": "87 rue de la Roquette, 75011 Paris", "phone": "(33)143 798 309"},
    {"name": "La Cave de l'Insolite", "address": "30 rue de la Folie Méricourt, 75011 Paris", "phone": "(33)153 360 833"}
]);


*/

// 🤔 arrays

#[wasm_bindgen]
pub fn handle(value: JsValue, human: JsValue) -> Result<JsValue, JsValue>  {
    // deserialize value (parameter) to question
    let question: Question = serde_wasm_bindgen::from_value(value)?;

    // HashMap<String, _>
    let human_map: HashMap<String,String> = serde_wasm_bindgen::from_value(human)?; // .unwrap()


    let some_data = match human_map.get("first") {
        Some(string) => format!("😁😡 first name: {}", string),
        _ => format!("no first name."),
    };


    let first_name: String = human_map.get("first").unwrap().to_string();
    let last_name: String = human_map.get("last").unwrap().to_string();
    
    //human_map.get(&"first").unwrap_or(&String::from("@k33g_org"));
    /*
    , human: Map
    let first_name = human.get(&"first".into()).as_string().unwrap();
    let last_name = human.get(&"last".into()).as_string().unwrap();
    */
    
    // let url = format!("https://api.github.com/repos/{}/branches/master", repo);

    
    //let my_map = Map::new();
    //my_map.set(&"text".into(), &"yo".into());

    // serialize answer to JsValue
    let answer = Answer {
        text: String::from(format!("hello {} / {} {} | {}", question.author, first_name, last_name, some_data)),
        author: String::from("@k33g_org"),
    };

    
    //return JsValue::from_serde(&answer).unwrap()

    Ok(serde_wasm_bindgen::to_value(&answer)?)
    //return Ok(JsValue::from_serde(&answer).unwrap())
}