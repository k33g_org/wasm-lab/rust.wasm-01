FROM gitpod/workspace-full

#RUN sudo apt-get update && \
#    sudo apt-get install gettext -y

USER gitpod

RUN brew install httpie && \
    brew install hey && \
    curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh 

#brew install exa && \
#brew install bat && \

